const { query } = require("../models"); // Import connection

class Goods {
  // Get all data of goods
  async getAllGoods(req, res) {
    try {
      // Find all data in goods
      const data = await query(
        "SELECT g.id, g.name as goodName, g.id_supplier as idSupplier, s.name as goodSupplier, g.price FROM goods g JOIN suppliers s ON g.id_supplier=s.id ORDER BY g.id",
        []
      );

      if (data.length === 0) {
        return res.status(404).json({ errors: ["Data of goods not found"] });
      }

      res.status(200).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }

  // Get detail of data goods
  async getDetailGood(req, res) {
    try {
      // Find all data in goods
      const data = await query(
        "SELECT g.id, g.name as goodName, g.id_supplier as idSupplier, s.name as goodSupplier, g.price FROM goods g JOIN suppliers s ON g.id_supplier=s.id WHERE g.id=?",
        [req.params.id]
      );

      // If data not exists
      if (data.length === 0) {
        return res.status(404).json({ errors: ["Data of goods not found"] });
      }

      res.status(200).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }

  // Create new data of goods
  async createGood(req, res) {
    try {
      /* Async Await */

      // Insert Data
      const insertData = await query(
        "INSERT INTO goods(name, price, id_supplier) VALUES (?, ?, ?)",
        [req.body.name, req.body.price, req.body.id_supplier]
      );

      // Print new data
      const data = await query("SELECT * FROM goods WHERE id=?", [
        insertData.insertId,
      ]);

      res.status(201).json({ data });
    } catch (error) {
      console.log(error);
      return res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }

  async updateGood(req, res) {
    try {
      /* Async Await */

      // Update Data
      const dataSupplier = await query("SELECT * FROM suppliers WHERE id=?", [
        req.body.id_supplier,
      ]);

      console.log(dataSupplier);

      if (dataSupplier.length === 0) {
        return res
          .status(404)
          .json({ errors: ["Data of suppliers not found"] });
      }

      const updateData = await query(
        "UPDATE goods SET name=?, price=?, id_supplier=? WHERE id=?",
        [req.body.name, req.body.price, req.body.id_supplier, req.params.id]
      );

      if (updateData.affectedRows === 0) {
        return res.status(404).json({ errors: ["Data of goods not found"] });
      }

      // Find Updated Data
      const data = await query("SELECT * from goods WHERE id=?", [
        req.params.id,
      ]);

      res.status(200).json({ data });
    } catch (error) {}
  }

  async deleteGood(req, res, next) {
    try {
      /* Async Await */

      //   Delete data
      const deleteData = await query("DELETE FROM goods WHERE id = ?", [
        req.params.id,
      ]);

      if (deleteData.affectedRows === 0) {
        return res.status(404).json({ errors: ["Data of goods not found"] });
      }

      res.status(200).json({ data: {} });
    } catch (error) {}
  }
}

module.exports = new Goods();
