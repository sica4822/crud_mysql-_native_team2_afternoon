const { query } = require("../models"); // Import connection

class Customers {
  // Get all data of customer
  async getAllCustomers(req, res) {
    try {
      // Find all data in goods
      const data = await query("SELECT * FROM customers", []);

      if (data.length === 0) {
        return res
          .status(404)
          .json({ errors: ["Customers name is not found"] });
      }

      res.status(200).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }

  // Get detail of data customer
  async getDetailCustomers(req, res) {
    try {
      // Find all data in customer
      const data = await query("SELECT * FROM customers WHERE id=?", [
        req.params.id,
      ]);

      // If data not exists
      if (data.length === 0) {
        return res
          .status(404)
          .json({ errors: ["Customers name is not found"] });
      }

      res.status(200).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }

  // Create new data in customer
  async createCustomers(req, res) {
    try {
      /* Async Await */

      // Insert Data
      const insertData = await query("INSERT INTO customers(name) VALUES (?)", [
        req.body.name,
      ]);

      // Print new data
      const data = await query("SELECT * FROM customers WHERE id=?", [
        insertData.insertId,
      ]);

      res.status(201).json({ data });
    } catch (error) {
      console.log(error);
      return res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }

  async updateCustomers(req, res) {
    try {
      /* Async Await */

      // Update Data
      const updateData = await query(
        "UPDATE customers SET name=?  WHERE id=?",
        [req.body.name, req.params.id]
      );

      if (updateData.affectedRows === 0) {
        return res
          .status(404)
          .json({ errors: ["Data of customers is not found"] });
      }

      // Find Updated Data
      const data = await query("SELECT * FROM customers WHERE id=?", [
        req.params.id,
      ]);

      res.status(200).json({ data });
    } catch (error) {}
  }

  async deleteCustomers(req, res, next) {
    try {
      /* Async Await */

      //   Delete data
      const deleteData = await query("DELETE FROM customers WHERE id = ?", [
        req.params.id,
      ]);

      if (deleteData.affectedRows === 0) {
        return res
          .status(404)
          .json({ errors: ["Data of customers is not found"] });
      }

      res.status(200).json({ data: {} });
    } catch (error) {}
  }
}

module.exports = new Customers();
