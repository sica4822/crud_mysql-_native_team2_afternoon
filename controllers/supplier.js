const { query } = require("../models"); // Import connection

class Suppliers {
  // Get all data of suppliers
  async getAllSuppliers(req, res) {
    try {
      // Find all data in suppliers
      const data = await query("SELECT * FROM suppliers", []);

      if (data.length === 0) {
        return res
          .status(404)
          .json({ errors: ["Data of suppliers not found"] });
      }

      res.status(200).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }

  // Get detail of data suppliers
  async getDetailSuppliers(req, res) {
    try {
      // Find all data in suppliers
      const data = await query("SELECT * FROM suppliers WHERE id=?", [
        req.params.id,
      ]);

      // If data not exists
      if (data.length === 0) {
        return res
          .status(404)
          .json({ errors: ["Data of suppliers not found"] });
      }

      res.status(200).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }

  // Create new data of suppliers
  async createSuppliers(req, res) {
    try {
      /* Async Await */

      // Insert Data
      const insertData = await query("INSERT INTO suppliers(name) VALUES (?)", [
        req.body.name,
      ]);

      // Print new data
      const data = await query("SELECT * FROM suppliers WHERE id=?", [
        insertData.insertId,
      ]);

      res.status(201).json({ data });
    } catch (error) {
      console.log(error);
      return res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }

  async updateSuppliers(req, res) {
    try {
      /* Async Await */

      // Update Data
      const updateData = await query("UPDATE suppliers SET name=? WHERE id=?", [
        req.body.name,
        req.params.id,
      ]);

      if (updateData.affectedRows === 0) {
        return res
          .status(404)
          .json({ errors: ["Data of suppliers not found"] });
      }

      // Find Updated Data
      const data = await query("SELECT * from suppliers WHERE id=?", [
        req.params.id,
      ]);

      res.status(200).json({ data });
    } catch (error) {}
  }

  async deleteSuppliers(req, res, next) {
    try {
      /* Async Await */

      //   Delete data
      const deleteData = await query("DELETE FROM suppliers WHERE id = ?", [
        req.params.id,
      ]);

      if (deleteData.affectedRows === 0) {
        return res
          .status(404)
          .json({ errors: ["Data of suppliers not found"] });
      }

      res.status(200).json({ data: {} });
    } catch (error) {}
  }
}

module.exports = new Suppliers();
