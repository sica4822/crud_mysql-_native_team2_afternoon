// Import express
const express = require("express");

// Import controller
const {
  getAllSuppliers,
  getDetailSuppliers,
  createSuppliers,
  updateSuppliers,
  deleteSuppliers,
} = require("../controllers/supplier");

// Make router
const router = express.Router();

// Router for getAllSuppliers
router.get("/", getAllSuppliers);

// Router for getDetailSuppliers
router.get("/:id", getDetailSuppliers);

// Router for createSuppliers
router.post("/", createSuppliers);

// Router for updateSuppliers
router.put("/:id", updateSuppliers);

// Router for deleteSuppliers
router.delete("/:id", deleteSuppliers);

module.exports = router; // Export router
