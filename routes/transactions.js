const express = require("express"); // Import express
// Import controller
const {
  createTransaction,
  getAllTransactions,
  getDetailTransaction,
  updateData,
  deleteData,
} = require("../controllers/transactions");

// Make router
const router = express.Router();

router.get("/", getAllTransactions);
router.get("/:id", getDetailTransaction);
router.post("/", createTransaction);
router.put("/:id", updateData);
router.delete("/:id", deleteData);

module.exports = router; // Export router
