// Import express
const express = require("express");

// Import controller
const {
  getAllGoods,
  getDetailGood,
  createGood,
  updateGood,
  deleteGood,
} = require("../controllers/goods");

// Make router
const router = express.Router();

// Router for getAllGoods
router.get("/", getAllGoods);

// Router for getDetailGood
router.get("/:id", getDetailGood);

// Router for createGood
router.post("/", createGood);

// Router for updateGood
router.put("/:id", updateGood);

// Router for deleteGood
router.delete("/:id", deleteGood);

module.exports = router; // Export router
