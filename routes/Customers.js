// Import express
const express = require("express");

// Import controller
const {
  getAllCustomers,
  getDetailCustomers,
  createCustomers,
  updateCustomers,
  deleteCustomers
} = require("../controllers/customers");

// Make router
const router = express.Router();

// Router for getAllGoods
router.get("/", getAllCustomers);

// Router for getDetailGood
router.get("/:id", getDetailCustomers);

// Router for createGood
router.post("/", createCustomers);

// Router for updateGood
router.put("/:id", updateCustomers);

// Router for deleteGood
router.delete("/:id", deleteCustomers);

module.exports = router; // Export router
