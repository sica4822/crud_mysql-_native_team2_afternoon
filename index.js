// Import express
const express = require("express");

// Import routes
const transactions = require("./routes/transactions");
const goods = require("./routes/goods");
const customers = require("./routes/Customers");
const suppliers = require("./routes/supplier");

// Define Port
const port = process.env.PORT || 3000;

// Make express app
const app = express();

/* Enable req.body */
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Make Routes for Tabel of Transactions
app.use("/transactions", transactions);

// Make Routes for Tabel of Goods
app.use("/goods", goods);

// Make Routes for Tabel of Customers
app.use("/customers", customers);

// Make Routes for Tabel of Suppliers
app.use("/suppliers", suppliers);

/* Run server */
app.listen(port, () => console.log(`Server running on ${port}`));
